import tkinter
from tkinter import messagebox
import random
import pyperclip
import json

# ---------------------------- PASSWORD GENERATOR ------------------------------- #
letters = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v',
           'w', 'x', 'y', 'z', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R',
           'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z']
numbers = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9']
symbols = ['!', '#', '$', '%', '&', '(', ')', '*', '+']


def generate_password():
    entry_password.delete(0, tkinter.END)
    password_letters = [random.choice(letters) for _ in range(random.randint(8, 10))]
    password_symbols = [random.choice(symbols) for _ in range(random.randint(2, 4))]
    password_numbers = [random.choice(numbers) for _ in range(random.randint(2, 4))]

    password_list = password_letters + password_symbols + password_numbers
    random.shuffle(password_list)

    password = ''.join(password_list)
    entry_password.insert(0, password)
    pyperclip.copy(password)


# ---------------------------- SAVE PASSWORD ------------------------------- #


def save():
    website = (entry_website.get()).capitalize()
    email = entry_email.get()
    password = entry_password.get()
    new_data = {
        website: {
            'email': email,
            'password': password,
        }
    }

    if len(website) == 0 or len(password) == 0:
        tkinter.messagebox.showerror(title='Error', message='Please consider typing both website and password.')
    else:
        try:
            with open('data.json', 'r') as data_file:
                data = json.load(data_file)
        except FileNotFoundError:
            with open('data.json', 'w') as data_file:
                json.dump(new_data, data_file, indent=4)
        else:
            data.update(new_data)

            with open('data.json', 'w') as data_file:
                json.dump(data, data_file, indent=4)

        finally:
            entry_website.delete(0, tkinter.END)
            entry_password.delete(0, tkinter.END)
            tkinter.messagebox.showinfo(title='Success', message='Your data were successfully saved.')


# ---------------------------- UI SETUP ------------------------------- #
window = tkinter.Tk()
window.title('Password Manager')
window.config(padx=50, pady=50)

canvas = tkinter.Canvas(height=200, width=200)
logo_png = tkinter.PhotoImage(file='logo.png')
canvas.create_image(100, 100, image=logo_png)
canvas.grid(column=1, row=0)


# Website label
website_label = tkinter.Label(text='Website:')
website_label.grid(column=0, row=1)


# Email/Username label
email_label = tkinter.Label(text='Email/Username:')
email_label.grid(column=0, row=2)


# Password label
password_label = tkinter.Label(text='Password:')
password_label.grid(column=0, row=3)


def aloha():
    asd = entry_website.get()
    print(asd)


# Entry website
entry_website = tkinter.Entry(width=35)
entry_website.focus()
entry_website.grid(column=1, row=1, columnspan=2, sticky="EW")


# Entry email
entry_email = tkinter.Entry(width=35)
entry_email.insert(0, 'jonyreeves@gmail.com')
entry_email.grid(column=1, row=2, columnspan=2, sticky="EW")


# Entry password
entry_password = tkinter.Entry(width=21)
entry_password.grid(column=1, row=3, sticky='EW')


# Generate password button
generate = tkinter.Button(text='Generate Password', command=generate_password)
generate.grid(column=2, row=3, sticky="EW")


# Add button
add = tkinter.Button(text='Add', width=36, command=save)
add.grid(column=1, row=4, columnspan=2, sticky="EW")


def find_password():
    website = (entry_website.get()).capitalize()
    email = entry_email.get()
    password = entry_password.get()
    new_data = {
        website: {
            'email': email,
            'password': password,
        }
    }
    try:
        with open('data.json', 'r') as data_file:
            data = json.load(data_file)
            if website in data:
                tkinter.messagebox.showinfo(title=website, message=f'Email: {data[website]["email"]}\nPassword: {data[website]["password"]}')
            else:
                tkinter.messagebox.showinfo(title='Error', message='No details for the website exists.')
    except FileNotFoundError:
        tkinter.messagebox.showinfo(title='Error', message='No Data File Found.')


# Button Search
search = tkinter.Button(text='Search', command=find_password)
search.grid(column=2, row=1, sticky='EW')







window.mainloop()